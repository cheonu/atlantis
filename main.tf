module "atlantis" {
  source  = "ringanta/ec2-atlantis/aws"
  version = "1.0.0"
  public_key          = "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABAQDKm2vUA7ooHI2VmSI5NEooLKexjcph/CzWqS8NQyPUnFEn781aqjQofTVsP/GMxtDjrZsXc1OjC94vdj/UKb9pSzgsL1qT3liCHn7maIXHW3XyDgiMrrDYQBGrRDcsBTxCqXJW4p4mJcXXWo5f58laEushpPqeOeOlJWfMsx2twTigyytFrjallv4S0i102wc7+nMX443iZ98KW/mOsdZ9XgmdW42dJFV2i+Sk8dV6QuclYPko0nxMouqUJygxg32XOXTmh5JiHZLH56fFIZiUScBNnpvhZqq70BKPUgpflG5x8ftgj2EJU2PSGvvi/InPq1O1vdP8oBXEZpMX1uRV"
  attach_admin_policy = true
}

resource "aws_s3_bucket" "terraform_state" {
  bucket = "terraform-s3-backendnew"
  versioning {
    enabled = true
  }
}

resource "aws_dynamodb_table" "terraform_lock" {
  name           = "terraform-lock"
  read_capacity  = 10
  write_capacity = 10
  hash_key       = "LockID"
  attribute {
    name = "LockID"
    type = "S"
  }
}