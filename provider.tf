terraform {
  required_version = "~> 0.13.0"
}

provider "aws" {
  region = "ca-central-1"
}

provider "random" {
  version = "~> 2.2"
}